**Capture The Wool - Resource Pack**<br/>
A fork of [Bleb Resources](https://www.curseforge.com/minecraft/texture-packs/bleb-resources)<br/>
For commerical use by [Blebi Studios](http://bleb.io/)

**Proudly open for community collaboration**<br/>
Create issues in the [issue tracker](https://gitlab.com/blebmc/blebtex-rftw-2/-/issues/new) to suggest changes<br/>
Browse the [history](https://gitlab.com/blebmc/blebtex-rftw-2/-/commits/master/) to see the development over time<br/>

Note that you should **NOT** download the pack in order to play on the Blebi servers.<br/>
The most recent stable resource packs for your gamemode **will be downloaded and applied by Minecraft automatically.**
